Dropzone.autoDiscover = false;

var pos_s = document.getElementsByClassName('pos_s')[0]

$('._menuTrig').on('click', function (e) {
    e.stopPropagation()
    if ($('._menuTrig').next('.dropM').hasClass('show')) {
        $('.dropM').removeClass('show')
        $('._menuTrig').attr("aria-expanded", "false");
    } else {
        $('.dropM').addClass('show')
        $('._menuTrig').attr("aria-expanded", "true");

        var activeItem = $('.level2.active').parents('ul.main_categories').outerHeight(),
            activeItemChild = $('.level2.active').find('.main_categories_wrap_right').outerHeight();

        if (activeItem < activeItemChild) {
            $('.level2.active').parents('ul.main_categories').css({
                "min-height": activeItemChild - 70
            });
        } else {
            $('.level2.active').parents('ul.main_categories').css({
                "min-height": activeItem
            });
        }

    }
})

$(document).on('click', function (e) {
    if ($('.dropM').hasClass('show') && !$(e.target).parents('.dropM').length && !$(e.target).parents('._menuTrig').length) {
        $('.dropM').removeClass('show')
        $('._menuTrig').attr("aria-expanded", "false");
    }
})

var disableScroll = false;
var scrollPos = 0;

function stopScroll() {
    disableScroll = true;
    scrollPos = $(window).scrollTop();
}

function enableScroll() {
    disableScroll = false;
}

jQuery(function ($) {

    $('.popupNavTrigger').on('click', function () {
        $(this).toggleClass('is-active');

        ps1 = new PerfectScrollbar('._scrollable', {
            wheelSpeed: 1,
            wheelPropagation: false,
            suppressScrollX: true
        });
    });

    $(window).bind('scroll', function () {
        if (disableScroll) $(window).scrollTop(scrollPos);
    });
    $(window).bind('touchmove', function () {
        $(window).trigger('scroll');
    });
    var ps1,
        ps2;
    $('._level_1 span').on('click', function (event) {
        event.stopPropagation();
        ps1 = new PerfectScrollbar('._scrollable', {
            wheelSpeed: 1,
            wheelPropagation: false,
            suppressScrollX: true
        });
    });
    $('.hasChild').on('click', function (event) {
        if (ps1) {
            ps1.destroy();
        }
        if (ps2) {
            ps2.destroy();
        }
        $(this).find('.childItem').first().addClass('active');
    });
    $('.childItem__goBackBtn span').on('click', function (event) {
        event.stopPropagation();
        $(this).parents('.childItem').first().removeClass('active');
    });
    $('.mPopupNavScroll').each(function (index, el) {
        const ps = new PerfectScrollbar(this, {
            wheelSpeed: 1,
            wheelPropagation: false,
            suppressScrollX: true
        });
    });
    $('._close').on('click', function () {});

    $('.mPopupNav').on('hide.bs.modal', function () {
        if (ps1) {
            ps1.destroy();
        }
        if (ps2) {
            ps2.destroy();
        }
        $('.popupNavTrigger').removeClass('is-active');
        $('._popupNav').find('.childItem').each(function () {
            $(this).removeClass('active');
        });
    })
    $('._pass_visibility_trig').on('click', function () {
        var pass = $(this).parents('.pass_field').find('#pass');
        if (pass.attr('type') == 'password') {
            pass.attr('type', 'text');
        } else {
            pass.attr('type', 'password');
        }
        $(this).find('.pas_icon_trig').toggle();
    })

    function scrollItAll(whatToScroll, whatToDo) {
        var ps1_scrollable_content;
        ps1_scrollable_content = new PerfectScrollbar(whatToScroll, {
            wheelSpeed: 1,
            wheelPropagation: false,
            suppressScrollX: true
        });
    }

    if ($('._scrollable_content').length) {
        $('._scrollable_content').each(function (index, el) {
            var k = '._scrollable_content' + index;
            scrollItAll(k)
        });
    }

    // + menu

    var $menu = $(".main_categories");
    $menu.menuAim({
        activate: activateSubmenu,
        deactivate: deactivateSubmenu
    });

    function activateSubmenu(row) {
        var $row = $(row);
        $row.addClass('active');
        var k1 = $row.parents('ul.main_categories').outerHeight(),
            k2 = $row.find('.main_categories_wrap_right').outerHeight();
        if (k1 < k2) {
            $row.parents('ul.main_categories').css({
                "min-height": k2 - 70
            });
        } else {
            $row.parents('ul.main_categories').css({
                "min-height": k1
            });
            // $row.parent('ul').height(k1);
        }
    }

    function deactivateSubmenu(row) {
        var $row = $(row);
        $row.removeClass('active');
        $row.parents('ul.main_categories').removeAttr('style')
    }

    // - menu

    //set defaults for modals
    var scrollWidth = window.innerWidth - document.documentElement.clientWidth;
    var modalIsOpen = false,
        modalIsAnimated = false;

    function simpleModalShow(target) {
        if ($(target).length && !modalIsAnimated) { //no act while animating is in proggress or no target specified
            modalIsOpen = true;
            $('body').addClass('modal-open');
            $('body').css('padding-right', scrollWidth);
            $(target).velocity("fadeIn", {
                duration: 300,
                begin: function () {
                    modalIsAnimated = true;
                    $(target).trigger('modal_show');
                },
                complete: function () {
                    modalIsAnimated = false;
                    $(target).trigger('modal_shown');
                }
            });
            $(target).removeClass('fade');
            $(target).addClass('show');
        } else {
            console.error('the target modal does not exist');
        }
    }

    function simpleModalHide() {

        if (!modalIsAnimated) { //no act while animating is in proggress
            $('.modal.show').velocity("fadeOut", {
                duration: 300,
                begin: function () {
                    modalIsAnimated = true;
                    $('.modal.show').trigger('modal_hide');
                },
                complete: function () {
                    $('body').removeClass('modal-open');
                    $('body').css('padding-right', '');
                    $('.modal.show').removeClass('show');
                    modalIsOpen = false;
                    modalIsAnimated = false;
                    $('.modal.show').trigger('modal_hidden');
                }
            });
        }
    }

    $(document).on('click', '.toggleModal', function (event) {
        event.preventDefault();
        var target = $(this).data('target');

        if (modalIsOpen) {
            simpleModalHide();
        } else {
            simpleModalShow(target);
            $('body').addClass('scrolledUp');
        }
    });

    $('.modal:not(._popupNav), ._closeBtn').on('click', function (event) {
        if ($(event.target).attr('class') == $(this).attr('class')) { //work around bubbling
            event.preventDefault();
            simpleModalHide();
        }
    });
    if ($('#formMainDropzone').length) {
        new Dropzone('#formMainDropzone',
            {
                autoProcessQueue: false,
                uploadMultiple: false,
                maxFiles: 9,
                dictDefaultMessage: ' ',
                createImageThumbnails: false,
                url: '/asasas',
                previewTemplate: '<div class="uploaded-image"><span data-dz-name></span> <strong class="dz-size" data-dz-size></strong><div class="dz-error-message" data-dz-errormessage></div><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div></div>'
            });
    }

});